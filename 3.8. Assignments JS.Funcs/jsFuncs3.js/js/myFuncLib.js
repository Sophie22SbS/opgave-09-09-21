// JavaScript Document

/* Celsius til Fahrenheit konverteringsformel er:
C = (5/9) * (F - 32) */

function c2f(c) 
{
  var cTemp = c;
  var c2f = cTemp * 9 / 5 + 32;
  var message = cTemp+'\xB0C is ' + c2f + ' \xB0F.';
    console.log(message);
}

function f2c(f) 
{
  var fTemp = f;
  var f2c = (fTemp - 32) * 5 / 9;
  var message = fTemp+'\xB0F is ' + f2c + '\xB0C.';
    console.log(message);
} 

c2f(60);
f2c(45);